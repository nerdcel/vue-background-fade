import { shallowMount } from '@vue/test-utils';
import BackgroundFade from '../../src/components/Component.vue';

describe('BackgroundFade.vue', () => {
  const images = ['image1', 'image2', 'image3', 'image4'];
  const interval = 1000;

  it('shuffles backgrounds after created', () => {
    const wrapper = shallowMount(BackgroundFade, {
      propsData: {
        images,
        interval,
      },
    });
    expect(wrapper.vm.shuffledImages)
      .not
      .toEqual(images);
  });

  it('increments index', () => {
    const wrapper = shallowMount(BackgroundFade, {
      propsData: {
        images,
        interval,
      },
    });

    const { index } = wrapper.vm;
    const indexNew = wrapper.vm.increment(index, images);

    expect(index)
      .not
      .toEqual(indexNew);
  });

  it('sets current and preview images', () => {
    const wrapper = shallowMount(BackgroundFade, {
      propsData: {
        images,
        interval,
      },
    });

    const { backgroundImage, nextImage } = wrapper.vm;

    wrapper.vm.setNext();
    const backgroundImageNew = wrapper.vm.backgroundImage;

    wrapper.vm.setPreview();
    const nextImageNew = wrapper.vm.nextImage;

    expect(backgroundImage)
      .not
      .toEqual(backgroundImageNew);
    expect(nextImage)
      .not
      .toEqual(nextImageNew);
  });

  it('predicts preview image', () => {
    const wrapper = shallowMount(BackgroundFade, {
      propsData: {
        images,
        interval,
      },
    });

    const previewImage = wrapper.vm.getPreviewImage();
    wrapper.vm.setNext();
    const { backgroundImage } = wrapper.vm;

    expect(backgroundImage)
      .toEqual(previewImage);
  });

  it('shuffles start/end after position change', () => {
    const wrapper = shallowMount(BackgroundFade, {
      propsData: {
        images,
        interval,
      },
    });

    const initialImages = wrapper.vm.backgroundImages;
    wrapper.vm.index = images.length;
    const reshuffledImages = wrapper.vm.backgroundImages;

    expect(initialImages)
      .toEqual(initialImages);
    expect(initialImages)
      .not
      .toEqual(reshuffledImages);
  });

  it('works using only one image', () => {
    const wrapper = shallowMount(BackgroundFade, {
      propsData: {
        images: [images[0]],
        interval,
      },
    });

    const { backgroundImages, nextImage } = wrapper.vm;

    expect(backgroundImages.length)
      .toBe(2);
    expect(nextImage)
      .not
      .toBeNull();
  });

  it('to warn for missing properties', () => {
    const spy = jest.spyOn(global.console, 'error')
      .mockImplementation(() => {
      });
    shallowMount(BackgroundFade);
    expect(spy)
      .toBeCalled();
    expect(spy.mock.calls[0][0])
      .toContain('[Vue warn]: Missing required prop');
    spy.mockRestore();
  });
});
