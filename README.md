# vue-background-fade

Background image fader.

## Usage

run:  
```
npm i --save @nerdcel/vue-background-fade 
or  
yarn add @nerdcel/vue-background-fade
```

### In your Vue main.js:

```
import Vue from 'vue';  
import BackgroundFade from '@nerdcel/vue-background-fade';

Vue.use(BackgroundFade);
```

### In your template
```
<background-fade :images=[...your_values] 
                 :interval="5000" 
                  transition="fade" 
                  mode='fixed'
></background-fade>
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Jest Unit test
```
yarn test:unit
```
