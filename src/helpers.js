const helpers = {
  shuffle(array = []) {
    return Object.freeze(array)
      .map(a => [Math.random(), a])
      .sort((a, b) => a[0] - b[0])
      .map(a => a[1]);
  },
};

const { shuffle } = helpers;

export default helpers;
export {
  shuffle,
};
