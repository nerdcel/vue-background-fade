import Vue from 'vue';
import Component from './components/index';
import App from './App.vue';

Vue.use(Component);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
